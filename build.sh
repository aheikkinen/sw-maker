#!/bin/bash

sBaseDir=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
sDistDir="${sBaseDir}/dist"
sSourceFiles="${sBaseDir}/extensions/SW Maker"
sExtFileName=$(echo "${sSourceFiles##*/}")
sPackageFile="${sDistDir}/${sExtFileName}.ext"

rm -rf "${sDistDir}"
mkdir -p "${sDistDir}"

(cd "${sSourceFiles}" && zip -r "${sPackageFile}" . -x "*.DS_Store")
