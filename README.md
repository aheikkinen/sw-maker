
# NPC Maker

## Known supported sources

**PDFs (Books with + mark are extra-well supported):**

- Realms of Cthulhu (+)
- Interface Zero (+)
- Pirates of Spannish Main (+)
- Science Fiction Companion (+)
- The Last Parsec (+)
- Solomon Kane
- War of the Dead
- Tour of Darkness
- Beasts & Barbarians
- Weird War Rome
- Blood Legacy of Mars
- East Texas University

**Zadmar's monster collection** 

- https://www.peginc.com/forum/viewtopic.php?t=40885
- http://www.godwars2.org/SavageWorlds/monsters.html

...and any other monster created with Zadmar's converter tools:

- http://www.godwars2.org/SavageWorlds/convert.html
- http://www.godwars2.org/SavageWorlds/convert2.html

**Savagepedia**

- http://savagepedia.wikispaces.com/Monsters+and+Mooks
	
**Savage Free Bestiary:**

- http://goo.gl/7DCkD
	
**Fallout Bestiary**

- http://www.peginc.com/forum/viewtopic.php?t=34126
	
