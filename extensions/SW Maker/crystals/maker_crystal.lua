--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	Comm.registerSlashHandler("makecrystal", makeCrystalSlashCommand) 
	Comm.registerSlashHandler("makecrystal-update", rebuildCrystalsFromSource)  
	Comm.registerSlashHandler("makecrystal-clean", cleanBuildCrystals)  
	Comm.registerSlashHandler("makecrystal-remove", removeBuildCrystals)  

	HotkeyManager.registerHotkeyHandler("makecrystal", makeCrystalSlashCommand)
end

function makeCrystalSlashCommand()
	makeCrystalFromClipboardData()
end

function makeCrystalFromClipboardData(vTarget)
	CommonParser.createRecordsFromClipboardData(makeCrystal, vTarget)
end

function rebuildCrystalsFromSource()
	CommonParser.rebuildRecordsFromSource(makeCrystal, "crystals")
end

function cleanBuildCrystals(_,vFilter)
	CommonParser.cleanBuildRecords("crystals", vFilter)
end

function removeBuildCrystals(_,vFilter)
	CommonParser.removeBuildRecords("crystals", vFilter)
end

--
-- Create Crystal Record
--

function makeCrystal(sSource, vTarget, bCloseWinAfter)
	if not sSource then
		return
	end
	sSource = CommonParser.preFormat(sSource)

	local sDescription, nDescriptionStart = CrystalParser.getSection("Description:", sSource)
	local aPowerSection, nPowerSectionStart = CrystalParser.getPowersSection(sSource)
	local sContentSource = sSource:sub((nDescriptionStart and nDescriptionStart+1 or 1), nPowerSectionStart)
	local sTheme = CrystalParser.getSection("Theme:", sContentSource)
	local sBenefit = CrystalParser.getSection("Benefit:", sContentSource)
	local sDisposition = CrystalParser.getSection("Disposition:", sContentSource)

	local sHeaderSource = sSource:sub(1, (nDescriptionStart and nDescriptionStart-1))
	local sName = StringManager.trim(sHeaderSource)
	local nNameEnd = sHeaderSource:find('\n')
	if nNameEnd then
		sName = StringManager.trim(sHeaderSource:sub(1, nNameEnd-1))
	end
	sName = CommonParser.capitalizeString(sName)

	local getTargetNode = function(vTarget)
		if not vTarget then
			return DB.createNode("crystals").createChild()
		elseif type(vTarget) == "string" then
			return DB.createNode(vTarget).createChild()
		else
			return vTarget
		end
	end
	
	local nodeCrystal = getTargetNode(vTarget)
	
	DB.setValue(nodeCrystal, "locked", "number", 1)
	DB.setValue(nodeCrystal, "name", "string", sName)

	local sToken = "campaign/tokens/Crystal-" .. sName:gsub("%s+", "-") .. ".png"
	if Interface.isToken(sToken) then
		DB.setValue(nodeCrystal, "token", "token", sToken)
	else
		print("[WARN] No such token exists: " .. sToken)
	end
	
	local writeFormattedText = function(sField, sContent)
		if StringManager.isNotBlank(sContent) then
			DB.setValue(nodeCrystal, sField, "formattedtext", CommonParser.formatText(sContent))
		end
	end
	writeFormattedText("description", sDescription)
	writeFormattedText("theme", sTheme)
	writeFormattedText("benefits_description", sBenefit)
	writeFormattedText("dispositions_description", sDisposition)

	local writePower = function(rPower)
		local nodePower = nodeCrystal.createChild("powerlist").createChild()
		local sPowerDetails = rPower.sPower:match("%[([^%]]+)%]")
		local nodeRecord = CommonParser.findLibraryRecordByName("power", rPower.sPower:gsub("%[[^%]]+%]", ""))
		if nodeRecord then 
			DB.copyNode(nodeRecord, nodePower)
		else
			print("[WARN] Record with name [" .. rPower.sPower .."] was not found")
		end
		local sPowerName = CommonParser.capitalizeString(rPower.sPower)
		local sDescription = rPower.sDesc
		if nodeRecord then
			sPowerName = DB.getValue(nodeRecord, "name", "")
			sDescription = sDescription .. "\n\n" .. ("{{powerdesc::%s::**Original Power:** %s}}"):format(nodeRecord.getNodeName(), sPowerName)
		end
		if rPower.bSpecial then
			sPowerName = StringManager.append(sPowerName, "Special", ", ")
		end
		if StringManager.isNotBlank(sPowerDetails) then
			sPowerName = StringManager.append(sPowerName, "[" .. sPowerDetails .. "]", " ")
		end

		DB.setValue(nodePower, "source", "string", sPowerName)
        DB.setValue(nodePower, "description", "formattedtext", CommonParser.formatText(sDescription))
		DB.setValue(nodePower, "name", "string", rPower.sName)
		DB.setValue(nodePower, "rank", "string", "")
		DB.setValue(nodePower, "powerpoints", "string", "")
		if not StringManager.containsWord(DB.getValue(nodePower, "duration", ""), "Instant") then
			DB.setValue(nodePower, "duration", "string", "")
		end
		DB.setValue(nodePower, "trappings", "string", "")
		if rPower.bOngoing then
			DB.setValue(nodePower, "kind", "string", "Ongoing")
		end
		if rPower.bSelfOnly then
			DB.setValue(nodePower, "range", "string", "Self")
		end
		DB.setValue(nodePower, "modifier", "number", rPower.nModifier)
		for _,nodeEffect in pairs(DB.getChildren(nodePower, "effects")) do
			DB.setValue(nodeEffect, "durationtype", "string", "none")
			DB.setValue(nodeEffect, "duration", "number", "0")
		end
	end
	for _,rPower in pairs(CrystalParser.parsePowerSection(aPowerSection)) do
		writePower(rPower)
	end
	DB.setValue(nodeCrystal, "swmaker-source", "string", sSource)
	
	local win = Interface.openWindow("crystal", nodeCrystal.getNodeName())
	if win and bCloseWinAfter then
		win.close()
	end
end
