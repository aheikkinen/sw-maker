--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

reSectionStart = "%s*[%w%s+%-%[%];//\&']+:"
reFormattedSectionStart = "^%s*%*%*" .. reSectionStart .. "%s*%*%*"
rePlainSectionStart = "^[^%(%[{]" .. reSectionStart .. ""

rePowerLabel = "^%s*%*%*%s*[^%(]+%s*%*%*%s*%([^%)]+"

function getPowersSection(sSource)
	local _,nDispositionStart = getSection("Disposition:", sSource)
	if not nDispositionStart then
		return {}
	end
	local sSectionSource = sSource:sub(nDispositionStart+1) .. "\n"
	local nIndex = 0
	local aPowerLines = {}
	local nPowerSectionStart = nil
	while sSectionSource:find("\n", nIndex) do
		local nNextIndex = sSectionSource:find("\n", nIndex)
		local sLine = sSectionSource:sub(nIndex, nNextIndex-1)
		if sLine:find(rePowerLabel) and not nPowerSectionStart then
			nPowerSectionStart = nIndex + nDispositionStart - 1
		end
		if nPowerSectionStart then
			table.insert(aPowerLines, sLine)
		end
		nIndex = nNextIndex + 1
	end
	return aPowerLines, nPowerSectionStart
end

function getSection(sTitle, sSource)
	local parseSection = function(sTitle, sSource, sReSectionStart)
		local sSection = ""
		local _,nSectionStart, nSectionContentStart = CommonParser.getTerminatorFragment(sSource, sTitle)
		if not nSectionContentStart then
			return ""
		end
		local sSectionSource = sSource:sub(nSectionContentStart+1) .. "\n"
		local nIndex = 0
		local nUnclosedParenthesis = 0
		while sSectionSource:find("\n", nIndex) do
			local nNextIndex = sSectionSource:find("\n", nIndex)
			local sLine = sSectionSource:sub(nIndex, nNextIndex-1)
			if sLine:find(sReSectionStart) and nUnclosedParenthesis < 1 then
				return sSection, nSectionStart, nSectionContentStart
			else
				sSection = sSection .. "\n" .. sLine
			end
			nUnclosedParenthesis = nUnclosedParenthesis + CommonParser.getParenthesisState(sLine)
			nIndex = nNextIndex + 1
		end
		return sSection, nSectionStart, nSectionContentStart
	end
	local sFormattedTitle = "%*%*%s*" .. sTitle .. "%s*%*%*"
	local _,_,nFormattedStart = CommonParser.getTerminatorFragment(sSource, sFormattedTitle)
	if nFormattedStart then
		local sSection, nSectionStart, nSectionContentStart = parseSection(sFormattedTitle, sSource, reFormattedSectionStart)
		return sSection, nSectionStart, nSectionContentStart
	else
		local sSection, nSectionStart, nSectionContentStart = parseSection(sTitle, sSource, rePlainSectionStart)
		return sSection, nSectionStart, nSectionContentStart
	end
end

function parsePowerSection(aPowerSection)
	local aPowers = {}
	local sPower = ""
	local include = function(sPower)
		if StringManager.isNotBlank(sPower) then
			table.insert(aPowers, { sContent = sPower })
		end
	end
	for _,sLine in pairs(aPowerSection or {}) do
		if sLine:find(rePowerLabel) then
			include(sPower)
			sPower = sLine
		else
			sPower = sPower .. "\n" .. sLine
		end
	end
	include(sPower)
	for _,rPower in pairs(aPowers) do
		local sName, sInfo, sDesc = rPower.sContent:match("^%s*([^%(]+)%s*(%b())%s*[%.;]?%s*(.+)")
		rPower.sName = CommonParser.plainText(sName) or ""
		rPower.sPower = ""
		rPower.bOngoing = false
		rPower.nModifier = 0
		rPower.sDesc = sDesc or ""
		sInfo = sInfo and sInfo:gsub("^%(", ""):gsub("%)$", "")
		if StringManager.isNotBlank(sInfo) then
			for nIndex, sPart in pairs(StringManager.split(sInfo, ",", true)) do
				local sValue = CommonParser.plainText(sPart)
				if nIndex == 1 then
					rPower.sPower = sValue
				elseif tonumber(sValue) then
					rPower.nModifier = tonumber(sValue)
				elseif StringManager.equals(sValue, "special") then
					rPower.bSpecial = true
				else
					rPower.bOngoing = StringManager.containsWord(sValue, "ongoing")
				end
			end
		else
			print("[WARN] Failed to parse power properly: " .. rPower.sContent)
		end

		local nSelfOnlyStart, nSelfOnlyEnd = rPower.sDesc:lower():find("^%s*self%s*only[%.;]%s*")
		if nSelfOnlyStart and nSelfOnlyEnd then
			rPower.bSelfOnly = true
			rPower.sDesc = rPower.sDesc:sub(nSelfOnlyEnd+1)
		end
	end
	return aPowers
end
