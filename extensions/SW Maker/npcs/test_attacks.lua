--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

local getAttackInfoTests = {
	["Str+d4"] = {
		range = nil,
		damage = "Str+d4",
		armorpiercing = nil,
		rof = nil,
	},
	["RoF: 1, Range 30/60/120, Damage: 1-3d10, Shots: 48, Auto, 3RB). A 1 on the skill die, regardless of Wild Die, means the gun is burned out."]= {
		range = "30/60/120",
		damage = "3d10",
		armorpiercing = nil,
		rof = "1",
	},
	["[Range 12/24/48; Damage 2d8+1; Shots 15; AP 4, Semi-Auto"]= {
		range = "12/24/48",
		damage = "2d8+1",
		armorpiercing = 4,
		rof = nil,
	},
	["  Range: 12/24/48,Damage: 2d8, 10 actions to reload natrof 2 is good and has rof 3"]= {
		range = "12/24/48",
		damage = "2d8",
		armorpiercing = nil,
		rof = "3",
	},
	["   Str+8  AP:3    RANGE:   1/2/3  ROF6"]= {
		range = "1/2/3",
		damage = "Str+8",
		armorpiercing = 3,
		rof = "6",
	},
	["The toxic roach can spew a stream of highly acidic vomit (Range: 3/6/12). Targets must make Vigor roll at -2. Failure indicates they take 2d6 damage every round until the goo is removed from their body. Removing the goo takes 1 full round, during which the character may not move or perform any other actions."]= {
		range = "3/6/12",
		damage = "2d6",
		armorpiercing = nil,
		rof = nil,
	},
	["here is something for you str + d8 damage if you succeed. As well there is Str+d12+4 if you raise!"] = {
		range = nil,
		damage = "str+d8",
		armorpiercing = nil,
		rof = nil,
	},
	["here is something for you str + 2 damage if you succeed."] = {
		range = nil,
		damage = "Str+2",
		armorpiercing = nil,
		rof = nil,
	},
	["This gunap 5 has (AP:2)"] = {
		range = nil,
		damage = nil,
		armorpiercing = 2,
		rof = nil,
	},
	["This gunap 5 has AP 2"] = {
		range = nil,
		damage = nil,
		armorpiercing = 2,
		rof = nil,
	},
	["STR + d8 + 1"] = {
		range = nil,
		damage = "Str+d8+1",
		armorpiercing = nil,
		rof = nil,
	},
	["STR + 2"] = {
		range = nil,
		damage = "Str+2",
		armorpiercing = nil,
		rof = nil,
	},
	["Damage: 3d8+1 AP 2"] = {
		range = nil,
		damage = "3d8+1",
		armorpiercing = 2,
		rof = nil,
	},
	["2d6 damage"] = {
		range = nil,
		damage = "2d6",
		armorpiercing = nil,
		rof = nil,
	},
	["Str+4 damage"] = {
		range = nil,
		damage = "Str+4",
		armorpiercing = nil,
		rof = nil,
	},
	["Throwing; range 3/6/12; Str+d6"] = {
		range = "3/6/12",
		damage = "Str+d6",
		armorpiercing = nil,
		rof = nil,
	},
	["Str+d6; Reach 1'; Parry +1"] = {
		range = nil,
		damage = "Str+d6",
		armorpiercing = nil,
		rof = nil,
	},
	["Str; Reach 1\"; Constrict."] = {
		range = nil,
		damage = "Str",
		armorpiercing = nil,
		rof = nil,
	},
	[""] = {
		range = nil,
		damage = nil,
		armorpiercing = nil,
		rof = nil,
	},
}

function runTests()
	local nTestsFailed = 0
	for sSource,rExpected in pairs(getAttackInfoTests) do
		local rActual = StatBlockParser.getAttackInfo(sSource, nil, true)
		local p = function(sSource)
			if not sSource or type(sSource) ~= "string" then
				return sSource
			else
				return sSource:lower() 
			end
		end
		local assertValues = function(aFailures, sTitle, expected, actual)
			if p(expected) ~= p(actual) then
				table.insert(aFailures, "  " .. sTitle .. " '" .. tostring(expected) .. "' ~= '" .. tostring(actual) .. "'")
			end
		end
		local aFailures = {}
		assertValues(aFailures, "Range", rExpected.range, rActual.range)
		assertValues(aFailures, "Damage", rExpected.damage, rActual.damage)
		assertValues(aFailures, "AP", rExpected.armorpiercing, rActual.armorpiercing)
		assertValues(aFailures, "ROF", rExpected.rof, rActual.rof)
		if #aFailures > 0 then
			print("[TEST FAILURE]: source '" .. sSource .. "'")
			for _, sFailure in pairs(aFailures) do 
				print(sFailure) 
			end
			nTestsFailed = nTestsFailed + 1
		end
	end
	if nTestsFailed < 1 then
		print("[TESTS]: All tests passed!")
	end
end
