--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function onInit()
	Comm.registerSlashHandler("makenpc", makeNPCSlashCommand) 
	Comm.registerSlashHandler("makenpc-update", rebuildNPCsFromSource)  
	Comm.registerSlashHandler("makenpc-clean", cleanBuildNPCs)  
	Comm.registerSlashHandler("makenpc-remove", removeBuildNPCs)  
	Comm.registerSlashHandler("makenpc-module", makeModuleSlashCommand)
	Comm.registerSlashHandler("makenpc-tests", AttackStatTests.runTests)
	
	HotkeyManager.registerHotkeyHandler("makenpc", makeNPCSlashCommand)
end

function makeNPCSlashCommand()
	makeNPCFromClipboardData()
end

function makeNPCFromClipboardData(vTarget)
	CommonParser.createRecordsFromClipboardData(makeNPC, vTarget)
end

function rebuildNPCsFromSource()
	CommonParser.rebuildRecordsFromSource(makeNPC, "npc")
end

function cleanBuildNPCs(_,vFilter)
	CommonParser.cleanBuildRecords("npc", vFilter)
end

function removeBuildNPCs(_,vFilter)
	CommonParser.removeBuildRecords("npc", vFilter)
end

function makeModuleSlashCommand()
	Interface.openWindow("makemoduletool", "makemodulesettings")
end

function makeNPC(sSource, vTarget, bCloseWinAfter)
	if not sSource then
		return
	end
	sSource = CommonParser.preFormat(sSource)

	local isAttack = function(rAttack)
		if rAttack.damage then
			local nWeight = rAttack.damageWeight or -10
			for _,v in pairs({rAttack.range, rAttack.armorpiercing, rAttack.rof, rAttack.reach, rAttack.parry, rAttack.template}) do
				nWeight = nWeight + 1
			end
			return nWeight > 0
		end
		return false
	end
	
	local writeWeaponInfo = function(nodeWeaponlist, sName, rAttack, bHeavyWeapon)
		if isAttack(rAttack) then
			local appendToStringNode = function(node, sName, sNewValue)
				local sCurrent = DB.getValue(node, sName, "")
				if #sCurrent > 0 then
					sCurrent = sCurrent .. ", "
				end
				DB.setValue(node, sName, "string", sCurrent .. sNewValue)
			end
			local nodeWeapon = nodeWeaponlist.createChild()
			sName = StringManager.capitalize(StringManager.trim(sName))
			DB.setValue(nodeWeapon, "name", "string", sName)
			DB.setValue(nodeWeapon, "damage", "string", rAttack.damage)
			DB.setValue(nodeWeapon, "bonusdamage", "string", "d6")
			if bHeavyWeapon then
				appendToStringNode(nodeWeapon, "notes", "HW")
			end
			if rAttack.range then
				DB.setValue(nodeWeapon, "range", "string", rAttack.range)
			end
			if rAttack.reach then
				DB.setValue(nodeWeapon, "reach", "number", rAttack.reach)
			end
			if rAttack.parry then
				appendToStringNode(nodeWeapon, "notes", "Parry " .. rAttack.parry)
			end
			if rAttack.template then
				appendToStringNode(nodeWeapon, "notes", rAttack.template)
			end
			if rAttack.armorpiercing then
				DB.setValue(nodeWeapon, "armorpiercing", "number", rAttack.armorpiercing)
			end
			if rAttack.rof then
				DB.setValue(nodeWeapon, "rof", "string", rAttack.rof)
			end
			if rAttack.text then
				DB.setValue(nodeWeapon, "text", "formattedtext", "<p>" .. rAttack.text .. "</p>")
			end
		end
	end
	
	-- Attributes
	local aAttributes = {}
	local sAttributeLine, nAttributeLineStarts = StatBlockParser.getAttributesLine(sSource)
	local nSmartsAnimal = 0
	if sAttributeLine then
		aAttributes = {
			["agility"] = sAttributeLine:upper():match("AGILITY" .. StatBlockParser.reAttributeTrait),
			["smarts"] = sAttributeLine:upper():match("SMARTS" .. StatBlockParser.reAttributeTrait),
			["spirit"] = sAttributeLine:upper():match("SPIRIT" .. StatBlockParser.reAttributeTrait),
			["strength"] = sAttributeLine:upper():match("STRENGTH" .. StatBlockParser.reAttributeTrait),
			["vigor"] = sAttributeLine:upper():match("VIGOR" .. StatBlockParser.reAttributeTrait)
		}
		local sSmartsAnimal = sAttributeLine:upper():match("SMARTS[:%s]+D?[%-—%d]+[+%-]?%d*%s*%(?(%w?)%)?")
		if sSmartsAnimal and sSmartsAnimal:upper() == "A" then
			nSmartsAnimal = 1                                                                                                
		end
	end

	-- Derived stats
	local aDerivedStats = {}
	local sDerivedStatLine = StatBlockParser.getDerivedStatsLine(sSource)
	if sDerivedStatLine then
		local getDerivedStatValue = function(sSource, sDerStart1, sDerStart2)
			local getDerivedValue = function(sSource, sDerStart)
					if sDerStart then
						return sSource:upper():match(sDerStart:upper().."[%s:]+%+?([%d%-]+)")
					end
				end
			return tonumber(getDerivedValue(sSource, sDerStart1) or getDerivedValue(sSource, sDerStart2)) or 0
		end
		local nToughness, nArmor = sDerivedStatLine:upper():match("TOUGHNESS[%s:]+%+?([%d%-]+)%s*%(?(%d*)%)?")
		aDerivedStats = {
			["charisma"] = getDerivedStatValue(sDerivedStatLine, "Charisma", "Cha"),
			["pace"] =  getDerivedStatValue(sDerivedStatLine, "Pace"),
			["parry"] =  getDerivedStatValue(sDerivedStatLine, "Parry"),
			["toughness"] = tonumber(nToughness) or 0,
			["armor"] = tonumber(nArmor) or 0,
			["grit"] =  getDerivedStatValue(sDerivedStatLine, "Grit"),
			["strain"] =  getDerivedStatValue(sDerivedStatLine, "Strain"),
			["sanity"] =  getDerivedStatValue(sDerivedStatLine, "Sanity"),
			["corruption"] =  getDerivedStatValue(sDerivedStatLine, "Corruption"),
			["terror"] =  getDerivedStatValue(sDerivedStatLine, "Terror"),
			["firewall"] =  getDerivedStatValue(sDerivedStatLine, "Firewall"),
			["streetcred"] =  getDerivedStatValue(sDerivedStatLine, "Street Cred"),
			["reason"] =  getDerivedStatValue(sDerivedStatLine, "Reason"),
			["status"] =  getDerivedStatValue(sDerivedStatLine, "Status"),
		}
	end

	-- Validation: at least these information must be found
	if not sAttributeLine then
		Comm.addChatMessage({text = "WARNING, Cannot convert! Your input source is malformed: No valid Attributes", font = "systemfont", icon = "poll_negative"})
		return
	end

	if not sDerivedStatLine then
		Comm.addChatMessage({text = "WARNING, Cannot convert! Your input source is malformed: No valid Derived Stats", font = "systemfont", icon = "poll_negative"})
		return
	end

	-- Name, Description, Type (Pathfinder), Faction and Wild Card
	local nHeadEnd = nAttributeLineStarts - 1
	local sType, nTypeStart = StatBlockParser.getOtherListLine("Type:", sSource)
	if nTypeStart and nTypeStart < nHeadEnd then
		nHeadEnd = nTypeStart - 6
	end
	local sHeaderSource = sSource:sub(1, nHeadEnd)
	local sName = ""
	local sDescription = ""
	local nNameEnd = sHeaderSource:find('\n')
	if nNameEnd then
		sName = StringManager.trim(sHeaderSource:sub(1, nNameEnd-1))
		sDescription = sHeaderSource:sub(nNameEnd+1):gsub("\r", "")
		sDescription = StringManager.trim(sDescription)
	else
		sName = StringManager.trim(sHeaderSource)
	end
	local nWildCard = 0
	if sName:find("^%?") or sName:find("^!") then
		nWildCard = 1
		sName = sName:gsub("^[%?!]%s*", "")
	elseif sName:upper():find("%s*%(WILD CARD%)") then
		nWildCard = 1
		sName = sName:sub(1,sName:upper():find("%s*%(WILD CARD%)")-1)
	elseif sName:upper():find("%s*%(WC%)") then
		nWildCard = 1
		sName = sName:sub(1,sName:upper():find("%s*%(WC%)")-1)
	end

	local reDefeatedMarker = "^%[([^%]]+)%]%s*"
	local sDefeatedMarker = nil
	if sName:find(reDefeatedMarker) then
		local sDefeatedMarkerValue = sName:match(reDefeatedMarker)
		for _,sSetName in pairs(TokenMarkerManager.getDefeatedMarkerSetNames()) do
			if StringManager.equals(sSetName, sDefeatedMarkerValue) then
				sDefeatedMarker = sSetName
				break
			end
		end
		sName = sName:gsub(reDefeatedMarker, "")
	end
	
	sName = CommonParser.capitalizeString(sName)
	sDescription = CommonParser.capitalizeString(sDescription)
	
	-- Language (Pathfinder)
	local sLanguages, nLanguageStart = StatBlockParser.getOtherListLine("Languages:", sSource)
	if nLanguageStart then
		if not sSource:sub(nLanguageStart):find(":") then
			sSource = sSource:sub(0, nLanguageStart-1)
		end
	end

	-- Skills
	local aSkills = {}
	for sName,sTrait in string.gmatch(StatBlockParser.getSkillsLine(sSource), "([%w%s+%-*%[%]()//\;&']+)[:%s]+(d%d+[+%-]?%d*)") do 
		if sName and sTrait then
			sName = StringManager.trim(sName or "") 
			sTrait = StringManager.trim(sTrait or "")
			if sName ~= "" and sTrait ~= "" then
				table.insert(aSkills, {name = sName, trait = sTrait})
			end
		end
	end
	
	-- Conviction
	local sConviction = StatBlockParser.getOtherListLine("Conviction:", sSource) or "0"
	local nConviction = tonumber(sConviction) or 0

	-- Gear
	local aGears, sGearLine = StatBlockParser.getGearInfo(sSource)

	-- Lists
	local aEdges = {}
	local aHindrances = {}
	local aArcaneBackgrounds = {}

	-- Special Abilities
	local aSpecialAbilities = {}
	local sSpecialAbilityLines, nSpecialAbilityStart = StatBlockParser.getSpecialAbilitiesLines(sSource)
	for _,sAbilityLine in pairs(sSpecialAbilityLines) do
		local sName, sDesc = sAbilityLine:match(StatBlockParser.reTitleAndDescription)
		sDesc = sDesc or ""
		if sName then
			sName = StringManager.trim(sName or "")
			sDesc = StringManager.trim(sDesc:gsub('•',''))
			if StringManager.isNotBlank(sName) then
				local bAdd = true
				if StringManager.equals(sName, "Innate Powers") then
					local aPowers = StatBlockParser.getOtherListLineEntries("Innate Powers:", sAbilityLine)
					if #aPowers > 0 then
						table.insert(aArcaneBackgrounds, {
							name = "Innate Powers",
							arcaneskilltype = "none",
							powerpointtype = "none",
							aPowers = aPowers
						})
					end
				elseif StringManager.equalsIgnoreBrackets(sName, "Class Abilities") then
					local sClassEdge = sName:match("%(([^%)]+)%)")
					if StringManager.isNotBlank(sClassEdge) then
						local nodeClassEdge = CommonParser.findLibraryRecordByName("edge", sClassEdge)
						if nodeClassEdge then
							for _,nodeAbility in pairs(DB.getChildren(nodeClassEdge, "abilities")) do
								local sName = DB.getValue(nodeAbility, "name", "")
								local sType = DB.getValue(nodeAbility, "type")
								if StringManager.containsWord(sType, Interface.getString("char_edge")) then
									table.insert(aEdges, sName)
								elseif StringManager.containsWord(sType, Interface.getString("char_hindrance")) then
									table.insert(aHindrances, sName)
								else
									table.insert(aSpecialAbilities, { name = sName, description = "" })
								end
							end
							bAdd = false
						end
					end
				end
				if bAdd then
					table.insert(aSpecialAbilities, { name = sName, description = sDesc })
				end
			end
		end
	end

	-- Super Powers
	local aSuperPowers = {}
	local sSuperPowerLines, nSuperPowerStart = StatBlockParser.getSuperPowersLines(sSource)
	for _,sSuperPowerLine in pairs(sSuperPowerLines) do
		local sName, sDesc = sSuperPowerLine:match(StatBlockParser.reTitleAndDescription)
		sDesc = sDesc or ""
		if sName then
			sName = StringManager.trim(sName or "")
			sDesc = StringManager.trim(sDesc:gsub('•',''))
			if sName ~= "" then
				table.insert(aSuperPowers, { name=sName, description=sDesc })
			end
		end
	end
	
	-- Other lists
	local sOtherSource = sSource 
	if nSuperPowerStart then
		sOtherSource = sOtherSource:sub(1, nSuperPowerStart-1)
	elseif nSpecialAbilityStart then
		sOtherSource = sOtherSource:sub(1, nSpecialAbilityStart-1)
	end
	for _,sEdge in pairs(StatBlockParser.getOtherListLineEntries("Edges:", sOtherSource)) do
		table.insert(aEdges, sEdge)
	end
	for _,sHindrance in pairs(StatBlockParser.getOtherListLineEntries("Hindrances:", sOtherSource)) do
		table.insert(aHindrances, sHindrance)
	end
	for _,rArcaneBackground in pairs(StatBlockParser.getPowers(sOtherSource)) do
		table.insert(aArcaneBackgrounds, rArcaneBackground)
	end
	local sTacticLine = StatBlockParser.getOtherListLine("Tactics:", sOtherSource)

	-- realms of cthulhu
	local sSpells = StatBlockParser.getOtherListLine("Spells:", sOtherSource)
	local sMinorGifts = StatBlockParser.getOtherListLine("Minor Gifts:", sOtherSource)
	local sDefiningInterests = StatBlockParser.getOtherListLine("Defining Interests:", sOtherSource)
	local sMentalAffection = StatBlockParser.getOtherListLine("Mental Affliction:", sOtherSource)
	
	-- 50 fathoms
	local sAirSpells = StatBlockParser.getOtherListLine("Air:", sOtherSource)
	local sEarthSpells = StatBlockParser.getOtherListLine("Earth:", sOtherSource)
	local sFireSpells = StatBlockParser.getOtherListLine("Fire:", sOtherSource)
	local sWaterSpells = StatBlockParser.getOtherListLine("Water:", sOtherSource)
	
	-- pirates of spanish main
	local sBootyLine = StatBlockParser.getOtherListLine("Booty:", sOtherSource)
	local sFameLine = StatBlockParser.getOtherListLine("Fame:", sOtherSource)

	-- hellfrost
	local sTreasureLine = StatBlockParser.getOtherListLine("Treasure:", sOtherSource)
	local sLanguageLine = StatBlockParser.getOtherListLine("Languages:", sOtherSource)
	
	-- interface zero 2.0
	local sOccupationLine = StatBlockParser.getOtherListLine("Occupation:", sOtherSource)
	local sContactsLine = StatBlockParser.getOtherListLine("Contacts:", sOtherSource)
	local sCyberwareLine = StatBlockParser.getOtherListLine("Cyberware:", sOtherSource)
	if StringManager.isBlank(sCyberwareLine) then
		sCyberwareLine = StatBlockParser.getOtherListLine("Cyberware %([^%)]+%):", sOtherSource)
	end
	local sEngramsLine = StatBlockParser.getOtherListLine("Engrams:", sOtherSource)
	
	-- Savagepedia
	local sGenreSetting = StatBlockParser.getOtherListLine("Setting/Genre:", sOtherSource)

	--
	-- CREATE NPC FROM PARSED DATA
	--
	
	-- remove existing node and create new one
	local cleanNodeList = function(nodeNPC, sNodename)
		local node = nodeNPC.getChild(sNodename)
		if node then
			node.delete()
		end
		return nodeNPC.createChild(sNodename)
	end

	local getTargetNode = function(vTarget)
		if not vTarget then
			return DB.createNode("npc").createChild()
		elseif type(vTarget) == "string" then
			return DB.createNode(vTarget).createChild()
		else
			return vTarget
		end
	end

	local hasHeavyArmorFeature = function(vSource)
		if vSource then
			local checkName = function(sName)
				return StringManager.equals(sName, "Gargantuan") or ArmorManager.hasHeavyArmorKeyword(sName)
			end
			if type(vSource) == "string" and checkName(vSource) then
				return true
			elseif type(vSource) == "table" and (checkName(vSource.name) or ArmorManager.hasHeavyArmorKeyword(vSource.description)) then
				return true
			end
		end
		return false
	end
	
	local nodeNPC = getTargetNode(vTarget)
	
	-- Name, Wild Card, Defeated Marker
	DB.setValue(nodeNPC, "locked", "number", 1)
	DB.setValue(nodeNPC, "name", "string", sName)
	DB.setValue(nodeNPC, "wildcard", "number", nWildCard)
	if StringManager.isNotBlank(sDefeatedMarker) then
		DB.setValue(nodeNPC, "defeatedmarker", "string", sDefeatedMarker)
	end
	if nConviction > 0 then
		DB.setValue(nodeNPC, "conviction", "number", nConviction)
	end
	
	-- Token
	if sName and #sName > 0 then
		local sLetter = sName:sub(1,1):lower():match("^([a-z])") or "z"
		local sTokenName = "tokens/Medium/" .. sLetter .. ".png@Letter Tokens"
		DB.setValue(nodeNPC, "token", "token", sTokenName)
	end
	
	-- Attributes
	for attribute,value in pairs(aAttributes) do
		local dice, mod = StringManager.convertStringToDice(value:lower())
		if #dice < 1 then dice = {"d4"} end
		DB.setValue(nodeNPC, attribute, "dice", dice)
		DB.setValue(nodeNPC, attribute.."Mod", "number", mod) 
	end
	DB.setValue(nodeNPC, "smartsAnimal", "number", nSmartsAnimal)
	
	-- Derived Stats
	for derivedstat,value in pairs(aDerivedStats) do
		DB.setValue(nodeNPC, derivedstat, "number", value)
	end
	
	-- Skills
	local nodeNPCSkills = cleanNodeList(nodeNPC, "skills")
	for _,rSkill in pairs(aSkills) do
		local nodeSkill =  nodeNPCSkills.createChild()
		local aDice, nMod = StringManager.convertStringToDice(rSkill.trait)
		DB.setValue(nodeSkill, "name", "string", rSkill.name)
		DB.setValue(nodeSkill, "skill", "dice", aDice)
		DB.setValue(nodeSkill, "skillmod", "number", nMod)
		CommonParser.writeRecordLink(nodeSkill, "skill", "sw_referenceskill", rSkill.name, true)
	end

	-- Heavy Weapons
	local bHeavyWeapon = false
	for _,rAbility in pairs(aSpecialAbilities) do
		if StringManager.equals(rAbility.name, "Gargantuan") then
			bHeavyWeapon = true
		end
	end
	
	local bAddUnarmed = true

	-- Weapon Attacks
	local nodeWeaponlist = cleanNodeList(nodeNPC, "weaponlist")
	for _,sGear in pairs(aGears) do
		local sName, sDetails = sGear:match(StatBlockParser.reGetGearTitleAndDescription)
		if sName and sDetails then
			writeWeaponInfo(nodeWeaponlist, sName, StatBlockParser.getAttackInfo(sDetails), bHeavyWeapon)
		end
	end
	
	-- Special Ability Attacks
	for _,ability in pairs(aSpecialAbilities) do
		local bAttack = StatBlockParser.isBestiaryAttack(ability.name)
		if bAttack then
			bAddUnarmed = false
		end
		writeWeaponInfo(nodeWeaponlist, ability.name, StatBlockParser.getAttackInfo(ability.description, bAttack), bHeavyWeapon)
	end

	-- Unarmed
	if bAddUnarmed then
		writeWeaponInfo(nodeWeaponlist, "Unarmed", {
			damage = "Str",
			damageWeight = 1
		})
	end
	
	-- Mental Anguish Attack
	local nAnguishStart = sSource:upper():find("MENTAL ANGUISH:?%s*[%w%s+%-()]")
	if nAnguishStart then
		local sAnguishLine = sSource:sub(nAnguishStart)
		local nAnguishEnd = sAnguishLine:find("\n")
		if nAnguishEnd then
			sAnguishLine = sAnguishLine:sub(0, nAnguishEnd-1)
		end
		local sAnguishDamage = sAnguishLine:upper():match("MENTAL ANGUISH:?%s*([%w%s+%-()]+)"):lower()
		for _,sAttribute in pairs(AttributeManager.getNodeNames()) do
			sAnguishDamage = sAnguishDamage:gsub(sAttribute:lower(), sAttribute:sub(1,3))
		end
		local nodeAnguish = nodeWeaponlist.createChild()     
		DB.setValue(nodeAnguish, "name", "string", "Mental Anguish")
		DB.setValue(nodeAnguish, "damage", "string", sAnguishDamage)
	end

	local bHeavyArmor = false

	-- Edges
	if #aEdges > 0 then
		local nodeEdges = nodeNPC.createChild("edges")
		for _,sEdge in pairs(aEdges) do
			if StringManager.isNotBlank(StringManager.simplify(sEdge)) then
				local nodeEdge = nodeEdges.createChild()
				local sDescription = ""
				local sReEdge = "([^%(]+)%(([^%)]+)%)"
				if sEdge:find(sReEdge) then
					local sEdgeName, sEdgeDescription = sEdge:match(sReEdge)
					DB.setValue(nodeEdge, "name", "string", sEdgeName)
					DB.setValue(nodeEdge, "description", "string", sEdgeDescription)
				else
					DB.setValue(nodeEdge, "name", "string", sEdge)
				end
				CommonParser.writeRecordLink(nodeEdge, "edge", "sw_referencefeat", sEdge, true)
			end
		end
	end

	-- Hindrances
	if #aHindrances > 0 then
		local nodeHindrances = nodeNPC.createChild("hindrances")
		for _,sHindrance in pairs(aHindrances) do
			if StringManager.isNotBlank(StringManager.simplify(sHindrance)) then
				local nodeHindrance = nodeHindrances.createChild()
				DB.setValue(nodeHindrance, "name", "string", sHindrance)
				CommonParser.writeRecordLink(nodeHindrance, "hindrance", "sw_referencefeat", sHindrance, true)
			end
		end
	end

	-- Special Abilities
	local nSpace = 1
	local nReach = 0
	local nodeNPCSpecial = cleanNodeList(nodeNPC, "special")
	for _,rAbility in pairs(aSpecialAbilities) do
		local nodeSpecial = nodeNPCSpecial.createChild()
		DB.setValue(nodeSpecial, "name", "string", rAbility.name)
		DB.setValue(nodeSpecial, "description", "string", rAbility.description)
		if not CommonParser.writeRecordLink(nodeSpecial, "monstrousability|edge|hindrance", "sw_referencefeat", rAbility.name, true) then
			DB.setValue(nodeSpecial, "type", "string", "Ability")
			DB.setValue(nodeSpecial, "benefit", "formattedtext", "<p>" .. rAbility.description .. "</p>")
		end
		if hasHeavyArmorFeature(rAbility) then
			bHeavyArmor = true
		end
		if rAbility.name:lower():find("size%s+%+?%d+") then
			local nSize = tonumber(rAbility.name:match("(%d+)") or "0")
			local getSpaceReach = function(nSize)
				for _,rSize in pairs(GameSystem.getSizes()) do
					if nSize >= rSize.nMinSize then
						return (rSize.nDefaultSpace or 1), (rSize.nReachMod or 0)
					end
				end
				return 1
			end
			nSpace, nReach = getSpaceReach(nSize)
		end
	end
	DB.setValue(nodeNPC, "space", "number", nSpace)
	DB.setValue(nodeNPC, "reach", "number", nReach)

	if bHeavyArmor then
		DB.setValue(nodeNPC, "heavyarmor", "number", 1)
	end

	-- Powers

	local writePower = function(sPower, vArcaneBackground)
		local nodePower = nodeNPC.createChild("powerlist").createChild()
		DB.setValue(nodePower, "name", "string", sPower)
		if vArcaneBackground and vArcaneBackground.name then
			DB.setValue(nodePower, "traittype", "string", vArcaneBackground.name)
		end
		local nodeRecord = CommonParser.writeRecordLink(nodePower, "power", "powerdesc", sPower, true)
		if nodeRecord then
			local writePowerValue = function(nodeSource, nodeTarget, sField, sFieldType, sDefault)
				local sValue = DB.getValue(nodeSource, sField, sDefault)
				if sValue ~= sDefault then
					DB.setValue(nodeTarget, sField, sFieldType, sValue)
				end
			end
			writePowerValue(nodeRecord, nodePower, "range", "string", "")
			writePowerValue(nodeRecord, nodePower, "powerpoints", "string", "")
			writePowerValue(nodeRecord, nodePower, "duration", "string", "")
			writePowerValue(nodeRecord, nodePower, "damage", "string", "")
			writePowerValue(nodeRecord, nodePower, "damagetype", "string", "")
			writePowerValue(nodeRecord, nodePower, "armorpiercing", "number", 0)
			writePowerValue(nodeRecord, nodePower, "effect", "string", "")
		end
		return nodePower
	end
	local findCharArcaneBackground = function(nodeNPC)
		local findArcaneBackground = function(sPath)
			for _,nodeAbility in pairs(DB.getChildren(nodeNPC, sPath)) do
				local sName = DB.getValue(nodeAbility, "name")
				if StringManager.equalsIgnoreBrackets(sName, Interface.getString("char_arcanebackground")) then
					return nodeAbility
				end
			end
		end
		local nodeEdge = findArcaneBackground("edges")
		local sArcaneBackground = nodeEdge and DB.getValue(nodeEdge, "description")
		if StringManager.isBlank(sArcaneBackground) then
			local nodeAbility = findArcaneBackground("special")
			sArcaneBackground = nodeAbility and DB.getValue(nodeAbility, "name", ""):match("%(([^%)]+)%)")
		end
		if StringManager.isNotBlank(sArcaneBackground) then
			return sArcaneBackground, CommonParser.findLibraryRecordByName("arcanebackground", sArcaneBackground)
		end
	end
	for _,rArcaneBackground in pairs(aArcaneBackgrounds or {}) do
		local aPowers = rArcaneBackground.aPowers or {}
		if #aPowers > 0 then
			if StringManager.isBlank(rArcaneBackground.name) then
				rArcaneBackground.name, rArcaneBackground.nodeRecord = findCharArcaneBackground(nodeNPC)
			end
			local nodeAB = DB.createChild(nodeNPC, "arcanebackgrounds").createChild()
			for sField, vValue in pairs(rArcaneBackground) do
				local sType = type(vValue)
				if StringManager.contains({ "string", "number" }, sType) then
					DB.setValue(nodeAB, sField, sType, vValue)
				end
			end
			if rArcaneBackground.nodeRecord then
				for _,node in pairs(DB.getChildren(rArcaneBackground.nodeRecord)) do
					DB.setValue(nodeAB, node.getName(), node.getType(), node.getValue())
				end
			end
			for _,sPower in pairs(aPowers) do
				writePower(sPower, rArcaneBackground)
			end
		end
	end
	if #aSuperPowers > 0 then
		local nodeAB = DB.createChild(nodeNPC, "arcanebackgrounds").createChild()
		DB.setValue(nodeAB, "name", "string", "Super Powers")
		DB.setValue(nodeAB, "arcaneskilltype", "string", "none")
		for _,rSuperPower in pairs(aSuperPowers) do
			local reSuperPowerLevel = "%s*(%([^%)]+%))%s*"
			local sSuperPowerLevel = rSuperPower.name:match(reSuperPowerLevel) or ""
			local sSuperPowerName = rSuperPower.name:gsub(reSuperPowerLevel, "")
			local nodePower = writePower(sSuperPowerName)
			if nodePower then
				DB.setValue(nodePower, "traittype", "string", "")
				local sBaseSuperPowerName = DB.getValue(nodePower, "name", "")
				DB.setValue(nodePower, "name", "string", StringManager.append(sBaseSuperPowerName, sSuperPowerLevel, " "))
				if StringManager.isNotBlank(rSuperPower.description) then
					local sBaseSuperPowerEffect = DB.getValue(nodePower, "effect", "")
					DB.setValue(nodePower, "effect", "string", StringManager.append(sBaseSuperPowerEffect, rSuperPower.description, "; "))
					local rAttack = StatBlockParser.getAttackInfo(rSuperPower.description)
					if rAttack.damage then
						DB.setValue(nodePower, "damage", "string", rAttack.damage)
					end
					if rAttack.range then
						DB.setValue(nodePower, "range", "string", rAttack.range)
					end
					if rAttack.armorpiercing then
						DB.setValue(nodePower, "armorpiercing", "number", rAttack.armorpiercing)
					end
				end
			end
		end
	end
	
	-- Description
	local isEmptyContent = function(sContent)
		return not sContent or StringManager.trim(sContent) == ""
	end
	local makeTitleAndContent = function(sTitle, sContent)
		if not isEmptyContent(sContent) then
			return "<b>" .. sTitle .. ":</b>" .. sContent
		else
			return ""
		end
	end
	local isEmptyContentList = function(aList)
		if not aList then
			return true
		end
		for _,content in pairs(aList) do
			if not isEmptyContent(content) then
				return false
			end
		end
		return true
	end
	local makeList = function(sTitle, aList)
		local entry = ""
		if not isEmptyContentList(aList) then
			if not isEmptyContent(sTitle) then
				entry = entry .. "<p><b>" .. sTitle .. "</b></p>"
			end
			entry = entry .. "<list>"
			for _,entity in pairs(aList) do
				if not isEmptyContent(entity) then
					entry = entry .. "<li>" .. entity .. "</li>"
				end
			end
			entry = entry .. "</list>"
		end
		return entry
	end
	local makeLine = function(sTitle, sContent)
		if sContent and sContent ~= "" then
			return "<p>" .. makeTitleAndContent(sTitle, sContent) .. "</p>"
		else
			return ""
		end
	end
	local toParagraphs = function(sContent)
		local aParagraphs = {}
		if StringManager.isNotBlank(sContent) then
			local findLineBreak = function(nIndex)
				local nStart, nEnd = sContent:find("[\r\n]%s*[\r\n]", nIndex)
				return nStart, nEnd
			end
			local nIndex = 0
			while findLineBreak(nIndex) do
				local nStart, nEnd = findLineBreak(nIndex)
				table.insert(aParagraphs, sContent:sub(nIndex, nStart-1))
				nIndex = nEnd + 1
			end
			local sRemaining = sContent:sub(nIndex)
			if StringManager.isNotBlank(sRemaining) then
				table.insert(aParagraphs, sRemaining)
			end
		end
		return aParagraphs
	end
	
	local sText = ""
	if StringManager.isNotBlank(sDescription) then
		for _,sParagraph in pairs(toParagraphs(sDescription)) do
			sText = sText .. "<p>" .. sParagraph .."</p>"
		end
	end

	sText = sText .. makeLine("Spells", sSpells)
	sText = sText .. makeLine("Fame", sFameLine)
	sText = sText .. makeLine("Occupation", sOccupationLine)
	sText = sText .. makeLine("Contacts", sContactsLine)
	sText = sText .. makeLine("Cyberware", sCyberwareLine)
	sText = sText .. makeLine("Engrams", sEngramsLine)
	sText = sText .. makeLine("Booty", sBootyLine)
	sText = sText .. makeLine("Treasure", sTreasureLine)
	sText = sText .. makeLine("Languages", sLanguageLine)
	sText = sText .. makeLine("Defining Interests", sDefiningInterests)
	sText = sText .. makeLine("Mental Affliction", sMentalAffection)
	sText = sText .. makeLine("Minor Gifts", sMinorGifts)
	sText = sText .. makeList(nil, {
		makeTitleAndContent("Air", sAirSpells), 
		makeTitleAndContent("Earth", sEarthSpells), 
		makeTitleAndContent("Fire", sFireSpells), 
		makeTitleAndContent("Water", sWaterSpells)
	})	
	sText = sText .. makeLine("Tactics", sTacticLine)
	sText = sText .. makeLine("Setting/Genre", sGenreSetting)

	DB.setValue(nodeNPC, "text", "formattedtext", CommonParser.formatText(sText))
	
	-- Gear
	DB.setValue(nodeNPC, "gear", "string", StringManager.trim(sGearLine))
	
	-- Pathfinder
	if StringManager.isNotBlank(sType) then
		DB.setValue(nodeNPC, "type", "string", sType)
	end
	if StringManager.isNotBlank(sLanguages) then
		DB.setValue(nodeNPC, "languages", "string", sLanguages)
	end

	-- Save source for reference
	DB.setValue(nodeNPC, "swmaker-source", "string", sSource)
	
	-- Open window
	local win = Interface.openWindow("npc", nodeNPC.getNodeName())
	if win then
		local nodeRecord = win.getDatabaseNode()
		if nodeRecord then
			NPCManagerSW.initializeAttackTypes(nodeRecord)
			NPCManagerSW.initializePowers(nodeRecord)
			for _,nodeWeapon in ipairs(DB.getChildList(nodeNPC, "weaponlist")) do
				WeaponManager.convertStringToDamageDice("npc", nodeNPC, nodeWeapon)
			end
			for _,nodePower in ipairs(DB.getChildList(nodeNPC, "powerlist")) do
				WeaponManager.convertStringToDamageDice("npc", nodeNPC, nodePower)
			end
		end
		if bCloseWinAfter then
			win.close()
		end
	end
end

function makeModule(aProperties)

	local toTagName = function(sSource)
		return sSource:lower():gsub("%W", "")
	end
	
	local sTagName = toTagName(aProperties.name)
	if sTagName == "" then
		Comm.addChatMessage({text="Invalid module name", mode="story", font="chatfont", icon="poll_negative"})
		return
	end
	
	-- Library
	local librarynode = DB.createNode("library").createChild("bestiary_" .. sTagName)
	DB.setValue(librarynode, "name", "string", aProperties.name)
	DB.setValue(librarynode, "categoryname", "string", aProperties.category)
	
	local indexroots = {}
	local getOrCreateIndexRoot = function(sName)
		if not indexroots[sName] then
			local entriesroot = librarynode.createChild("entries").createChild(sTagName .. "_" .. toTagName(sName))
			DB.setValue(entriesroot, "librarylink", "windowreference", "sw_referenceindex", "..")
			DB.setValue(entriesroot, "name", "string", sName)
			DB.setValue(entriesroot, "description", "formattedtext", "<p></p>")
			indexroots[sName] = entriesroot.createChild("index")
		end
		return indexroots[sName]
	end

	-- Reference
	local referencenode = DB.createNode("reference_" .. sTagName)
	for _,sourcenode in pairs(DB.getChildren("npc")) do
		local nodename = toTagName(DB.getValue(sourcenode, "name", ""))
		if nodename ~= "" then
			local targetnode = referencenode.createChild(nodename)
			DB.copyNode(sourcenode, targetnode)
			if targetnode.getChild("swmaker-source") then
				targetnode.getChild("swmaker-source").delete()
			end
			
			-- create index link
			local category = sourcenode.getCategory()
			if category then
				category = category.name
			end
			if not category or StringManager.trim(category) == "" then
				category = "Bestiary"
			end
			local indexnode = getOrCreateIndexRoot(category).createChild(nodename)
			DB.setValue(indexnode, "listlink", "windowreference", "npc", targetnode.getNodeName())
			DB.setValue(indexnode, "name", "string", DB.getValue(sourcenode, "name", ""))   
		end
	end
	
	local getEntryTable = function(node)
		local libraryentrytable = {}
		libraryentrytable.import = node.getNodeName()
		if node.getCategory() then
			libraryentrytable.category = node.getCategory()
			libraryentrytable.category.mergeid = aProperties.mergeid
		end
		return libraryentrytable
	end
	local aExportNodes = {}
	aExportNodes[librarynode.getNodeName()] = getEntryTable(librarynode)
	aExportNodes[referencenode.getNodeName()] = getEntryTable(referencenode)

	local bRet = Module.export(aProperties.name, aProperties.category, aProperties.author, aProperties.file, aProperties.thumbnail, aExportNodes, {})			
	
	if bRet then  
		ChatManager.SystemMessage(Interface.getString("bestiary_export_message_success"))
	else
		ChatManager.SystemMessage(Interface.getString("bestiary_export_message_failure"))
	end
	
	-- clean up
	librarynode.delete()
	referencenode.delete()
end
