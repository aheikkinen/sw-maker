--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

aSupportedDerivedStats = { "PACE", "PARRY", "TOUGHNESS", "CHARISMA", "GRIT", "STRAIN", "SANITY", "CORRUPTION", "TERROR", "FIREWALL", "STREET CRED", "ACADEMICS", "REASON", "STATUS" }
aSupportedAttacks = { "CLAWS", "BITE", "BEAK", "TALONS", "TRUNK SLAP", "TUSKS", "KICK", "HORNS", "MANDIBLES", "GORE", 
		      "SPINES", "FANGS", "PUNCH", "HOOVES", "STING", "PROBOSCIS", "SLAM", "PINCERS", "TAIL SPIKE", 
                      "PINCERS", "PINCER", "TENTACLES", "TEETH" }

reAttributeTrait = "[:%s]+(D?[%-—%d]+[+%-]?%d*)"
reListStart = "^[^%(%[{]%s*[%w%s+%-%[%];//\&']+:"
reSpecialAbilitiesLabel = "Special Abilities:?"
reSuperPowersLabel = "[^%(]Super Powers:?"
reTitleAndDescription = "([^:]+)[:%s]+(.+)"
reGetGearTitleAndDescription = "([%w%s+%-%.//\&']+)%(([%w%s+%-,;%.://\'!?\"]+)%)"

function removeLinebreaks(s)
	if StringManager.isNotBlank(s) then
		s = s:gsub("[\n\r]", "")
		return s
	end
	return ""
end

function isBestiaryAttack(sLabel)
	if sLabel then
		for _,sAttackType in pairs(aSupportedAttacks) do
			if sLabel:upper() == sAttackType then
				return true
			end
		end
	end
end

function getDamageString(sSource, bTests)
	if bTests then print("-----") end
	local reStatPrefix = "[%(%[{%s,;%.]"
	local reStatPostfix = "[%)%]}%s,;%.]"
	local reDamageString = "[%s%d+%-D]+"
	local getPatternPosition = function(sSource, sPattern, nIndex)
		local sMatchSource = sSource:upper()
		local aPatterns = {
			"^" .. sPattern .. "$",
			"^" .. sPattern .. reStatPostfix,
			reStatPrefix .. sPattern .. reStatPostfix,
			reStatPrefix .. sPattern .. "$"
		}
		for _, rePattern in pairs(aPatterns) do
			local nStart, nEnd = sMatchSource:find(rePattern, nIndex)
			if nStart and nEnd then
				local nStrStart, nStrEnd = sMatchSource:sub(nStart, nEnd):find(sPattern)
				return nStart+nStrStart-1, nStart+nStrEnd-1, nEnd
			end
		end
	end
	
	local hasDice = function(sSource)
		return sSource:upper():find("%d*D%d+")
	end
	
	local hasOperation = function(sSource)
		return sSource:upper():gsub("%s", ""):find("[+%-]%d+")
	end
	
	local startsWithOperation = function(sSource)
		return sSource:upper():gsub("%s", ""):find("^[+%-]")
	end
	
	local endsWithOperation = function(sSource)
		return sSource:upper():gsub("%s", ""):find("[+%-]$")
	end
	
	local isValidDamageString = function(sSource)
		return not startsWithOperation(sSource) and not endsWithOperation(sSource)
	end
	
	local aMatches = {}
	for _,sAttribute in pairs(AttributeManager.getNodeNames()) do
	
		-- just attribute
		local nIndex = 0
		while nIndex do
			local nStart, nEnd, nNextIndex = getPatternPosition(sSource, sAttribute:sub(1,3):upper(), nIndex)
			if not nStart or not nEnd then
				break
			end
			local sMatchSource = sSource:sub(nStart, nEnd)
			if StringManager.trim(sMatchSource or "") ~= "" then
				table.insert(aMatches, { nStart = nStart, nEnd = nEnd, nWeight = 1 })
			end
			nIndex = nNextIndex + 1
		end
		
		-- attribute and damage string
		nIndex = 0
		while nIndex do
			local nStart, nEnd, nNextIndex = getPatternPosition(sSource, sAttribute:sub(1,3):upper() .. reDamageString, nIndex)
			if not nStart or not nEnd then
				break
			end
			local sMatchSource = sSource:sub(nStart, nEnd)
			if StringManager.trim(sMatchSource or "") ~= "" then
				local bValid = (hasDice(sMatchSource) or hasOperation(sMatchSource)) and isValidDamageString(sMatchSource) == true
				if bTests then print(tostring(bValid) .. ": " .. sMatchSource) end
				if bValid then
					local nWeight = 1
					if hasDice(sSource:sub(nStart, nEnd)) then
						nWeight = nWeight + 1
					end
					if hasOperation(sSource:sub(nStart, nEnd)) then
						nWeight = nWeight + 1
					end
					table.insert(aMatches, { nStart = nStart, nEnd = nEnd, nWeight = nWeight })
				end
			end
			nIndex = nNextIndex + 1
		end
	end
	
	-- just damage string
	local nIndex = 0
	while nIndex do
		local nStart, nEnd, nNextIndex = getPatternPosition(sSource, reDamageString, nIndex)
		if not nStart or not nEnd then
			break
		end
		local sMatchSource = sSource:sub(nStart, nEnd)
		if StringManager.trim(sMatchSource or "") ~= "" then
			local bValid = hasDice(sMatchSource) and isValidDamageString(sMatchSource) == true
			if bTests then print(tostring(bValid) .. ": " .. sMatchSource) end
			if bValid then
				local nWeight = 0
				if hasOperation(sSource:sub(nStart, nEnd)) then
					nWeight = nWeight + 1
				end
				table.insert(aMatches, { nStart = nStart, nEnd = nEnd, nWeight = nWeight })
			end
		end
		nIndex = nNextIndex + 1
	end

	local aWeightedMatches = {}
	for _, rMatch in pairs(aMatches) do
		if sSource:sub(1, rMatch.nStart - 1):upper():find("DAMAGE:?%s*$") or 
		   sSource:sub(rMatch.nEnd + 1):upper():find("%s*DAMAGE") then
		   rMatch.nWeight = rMatch.nWeight + 2
		end
		table.insert(aWeightedMatches, rMatch)
	end
	
	table.sort(aWeightedMatches, function(a,b)
		if a.nWeight == b.nWeight then
			return a.nStart < b.nStart
		else
			return a.nWeight > b.nWeight
		end
	end)
	
	if #aWeightedMatches > 0 then
		local rBestMatch = aWeightedMatches[1]
		local sDamageString = sSource:sub(rBestMatch.nStart, rBestMatch.nEnd)
		
		-- remove damage range
		local reDiceRange = "%d+-()%d+D%d+"
		local nStart, _, nCut = sDamageString:upper():find(reDiceRange)
		if nStart and nCut then
			sDamageString = sDamageString:sub(1, nStart-1) .. sDamageString:sub(nCut)
		end
		sDamageString = sDamageString:gsub("%s", "")
		
		return StringManager.capitalize(sDamageString:lower()), rBestMatch.nWeight
	end
end

function getTemplates(sSource)
	local reStatPrefix = "[%(%[{%s,;%.]"
	local reStatPostfix = "[%)%]}%s,;%.]"
	local aTemplatePatterns = {
		"CONE%s*TEMPLATE",
		"SMALL%s*BURST%s*TEMPLATE", 
		"MEDIUM%s*BURST%s*TEMPLATE",
		"LARGE%s*BURST%s*TEMPLATE",
		"SBT",
		"MBT",
		"LBT",
	}
	
	local getPatternPosition = function(sSource, sPattern, nIndex)
		local sMatchSource = sSource:upper()
		local aPatterns = {
			"^" .. sPattern .. "$",
			"^" .. sPattern .. reStatPostfix,
			reStatPrefix .. sPattern .. reStatPostfix,
			reStatPrefix .. sPattern .. "$"
		}
		for _, rePattern in pairs(aPatterns) do
			local nStart, nEnd = sMatchSource:find(rePattern, nIndex)
			if nStart and nEnd then
				local nStrStart, nStrEnd = sMatchSource:sub(nStart, nEnd):find(sPattern)
				return nStart+nStrStart-1, nStart+nStrEnd-1, nEnd
			end
		end
	end

	local aMatches = {}
	for _, sTemplatePattern in pairs(aTemplatePatterns) do
		local nIndex = 0
		while nIndex do
			local nStart, nEnd, nNextIndex = getPatternPosition(sSource, sTemplatePattern, nIndex)
			if not nStart or not nEnd then
				break
			end
			local sMatchSource = sSource:sub(nStart, nEnd)
			table.insert(aMatches, { nStart = nStart, nEnd = nEnd })
			nIndex = nNextIndex + 1
		end
	end
	
	table.sort(aMatches, function(a,b) return a.nStart < b.nStart end)
	
	if #aMatches > 0 then
		local sTemplates = ""
		for _,rMatch in pairs(aMatches) do
			if #sTemplates > 0 then
				sTemplates = sTemplates .. ", "
			end
			sTemplates = sTemplates .. sSource:sub(rMatch.nStart, rMatch.nEnd)
		end
		return sTemplates
	end
end

function getAttackInfo(sSource, bAttackForced, bTests)
	local reStatPrefix = "[%(%[{%s,;%.]"
	local sMatchSource = sSource:upper()

	local rAttack = { text = sSource }

	-- if source is dice string
	if not bAttackForced then
		local sMatchString = sSource:lower():gsub("%s", ""):gsub("[^%w+%-]", "")
		for _,attr in pairs(AttributeManager.getNodeNames()) do
			sMatchString = sMatchString:gsub(attr:sub(1,3), "")
		end
		bAttackForced = StringManager.isDiceString(sMatchString)
	end
	
	local getPatternPosition = function(sSource, sPattern)
		return sSource:find(reStatPrefix .. sPattern) or sSource:find("^" .. sPattern)
	end
	
	-- Range
	local nRangeStart, nRangeEnd = getPatternPosition(sMatchSource, "RANGE:?%s*%d+/%d+/%d+") or getPatternPosition(sMatchSource, "%s*%d+/%d+/%d+")
	if nRangeStart then
		rAttack.range = sSource:sub(nRangeStart,nRangeEnd):match("(%d+/%d+/%d+)")
	end
	
	-- Damage
	rAttack.damage, rAttack.damageWeight = getDamageString(sSource, bTests)
	
	-- Armor piercing
	local reAP = "AP:?%s*%d+"
	local nAPStart,nAPEnd = getPatternPosition(sMatchSource, reAP)
	if nAPStart then
		rAttack.armorpiercing = tonumber(sSource:sub(nAPStart,nAPEnd):match("(%d+)"))
	end
	
	-- Rate of fire
	local reRof = "ROF:?%s*%d+"
	local nRofStart,nRofEnd = getPatternPosition(sMatchSource, reRof)
	if nRofStart then
		rAttack.rof = sMatchSource:sub(nRofStart,nRofEnd):match("(%d+)")
	end
	
	-- Reach
	local reReach = "REACH:?%s*%d+"
	local nReachStart,nReachEnd = getPatternPosition(sMatchSource, reReach)
	if nReachStart then
		rAttack.reach = tonumber(sMatchSource:sub(nReachStart,nReachEnd):match("(%d+)"))
	end
	
	-- Parry
	local reParry = "PARRY:?%s*[+%-]?%s*%d+"
	local nParryStart,nParryEnd = getPatternPosition(sMatchSource, reParry)
	if nParryStart then
		rAttack.parry = sMatchSource:sub(nParryStart,nParryEnd):match("([+%-]?%s*%d+)"):gsub("%s", "")
	end
	
	-- Templates
	rAttack.template = getTemplates(sSource)
	
	return rAttack
end

function getGearInfo(sSource)
	local aGears = {}
	local sGearLine = ""
	local aGearLabels = {
		"Gear:", 
		"Gear & Equipment:", 
		"Equipment:",
	}
	                                                   
	local reGearContent = "[%w%s+%-%[%]()//\;&']+"
	local sMatchSource = sSource:upper()
	
	local addGear = function(aGears, sGear, sDesc)
		sGear = StringManager.trim(sGear or "")
		if sGear ~= "" then
			sDesc = StringManager.trim(sDesc or "")
			if sDesc ~= "" then
				sGear = sGear .. " (" .. sDesc .. ")"
			end
			table.insert(aGears, sGear)
		end
		return aGears
	end
	
	-- Savage Free Bestiary
	if sMatchSource:find("GEAR:") and not CommonParser.getTerminatorFragment(sSource, "Gear:"):find(reGearContent) then
		local _,_,nGearLineStart = CommonParser.getTerminatorFragment(sSource, "Gear:")
		local _,nSpecialLineStart = CommonParser.getTerminatorFragment(sSource, "Special Abilities:")
		if nSpecialLineStart and nGearLineStart and nSpecialLineStart > nGearLineStart then
			sGearLine = sSource:sub(nGearLineStart,nSpecialLineStart)
		elseif nGearLineStart then
			sGearLine = sSource:sub(nGearLineStart)
		end
		if nGearLineStart then
			local nIndex = 0
			while sGearLine:find("\n", nIndex) do
				local nNextIndex = sGearLine:find("\n", nIndex)
				local sGear, sDesc = sGearLine:sub(nIndex, nNextIndex):match(StatBlockParser.reTitleAndDescription)
				aGears = addGear(aGears, sGear, sDesc)
				nIndex = nNextIndex + 1
			end
		end
	else
		for _,sLabel in pairs(aGearLabels) do
			if sMatchSource:find(sLabel:upper()) then
				sGearLine = getOtherListLine(sLabel, sSource)
				break
			end
		end
		local _,nSpeciaAbilitieslStart = CommonParser.getTerminatorFragment(sGearLine, reSpecialAbilitiesLabel)
		if nSpeciaAbilitieslStart then
			sGearLine = sGearLine:sub(1, nSpeciaAbilitieslStart-1)
		end
		local _,nSuperPowersLineStart = CommonParser.getTerminatorFragment(sGearLine, reSuperPowersLabel)
		if nSuperPowersLineStart then
			sGearLine = sGearLine:sub(1, nSuperPowersLineStart-1)
		end
		for sGear, sDesc in string.gmatch(sGearLine, StatBlockParser.reGetGearTitleAndDescription) do
			aGears = addGear(aGears, sGear, sDesc)
		end
	end
	return aGears, removeLinebreaks(sGearLine)
end

function getAttributesLine(sSource)
	local containsAtLeast = function(sLine, nCount)	
		local nFoundCount = 0
		for _,attr in pairs(AttributeManager.getNodeNames()) do
			if sLine:upper():find(attr:upper() .. reAttributeTrait) then
				nFoundCount = nFoundCount + 1
			end
		end
		return nFoundCount >= nCount
	end
	local containsAllAttributes = function(sLine)
		return containsAtLeast(sLine, #AttributeManager.getNodeNames())
	end
	local sAttributeLine = ""
	local nAttributeLineStart = nil
	local nIndex = 0
	while(sSource:find("\n", nIndex)) do
		local nNextIndex = sSource:find("\n", nIndex)
		local sLine = sSource:sub(nIndex, nNextIndex-1)
		if containsAtLeast(sLine, 2) or sAttributeLine ~= "" then
			sAttributeLine = sAttributeLine .. " " ..sLine
			if not nAttributeLineStart then
				local nAttributeHeaderStarts = sSource:sub(1,nIndex):upper():find("ATTRIBUTES?:?%s*$")
				if nAttributeHeaderStarts then
					nAttributeLineStart = nAttributeHeaderStarts
				else
					nAttributeLineStart = nIndex
				end
			end
		end
		if containsAllAttributes(sAttributeLine) then
			return sAttributeLine, nAttributeLineStart, nNextIndex+1
		end
		nIndex = nNextIndex+1
	end
end

function getDerivedStatsLine(sSource)
	local containsAtLeast = function(sLine, nCount)	
		local nFoundCount = 0
		for _,derstat in pairs(aSupportedDerivedStats) do
			if sLine:upper():find(derstat .. "[%s:]+%+?[%d%-]+") then
				nFoundCount = nFoundCount + 1
			end
		end
		return nFoundCount >= nCount
	end
	local containsAllAttributes = function(sLine)
		return containsAtLeast(sLine, #aSupportedDerivedStats)
	end
	local sDerivedStatLine = ""
	local nDerivedStatLineStart = nil
	local nIndex = 0
	while(sSource:find("\n", nIndex)) do
		local nNextIndex = sSource:find("\n", nIndex)
		local sLine = sSource:sub(nIndex, nNextIndex-1)
		if containsAtLeast(sLine, 1) then
			sDerivedStatLine = sDerivedStatLine .. " " ..sLine
			if not nDerivedStatLineStart then
				nDerivedStatLineStart = nIndex
			end
		elseif nDerivedStatLineStart then
			return sDerivedStatLine
		end
		nIndex = nNextIndex+1
	end
	return sDerivedStatLine
end

function getSkillsLine(sSource)
	local sSkillLine = ""
	local _, _, nSkillLineStart = CommonParser.getTerminatorFragment(sSource, "Skills:")
	if not nSkillLineStart then
		return ""
	end
	local sSkillSource = sSource:sub(nSkillLineStart+1) .. "\n"
	local nIndex = 0
	while(sSkillSource:find("\n", nIndex)) do
		local nNextIndex = sSkillSource:find("\n", nIndex)
		local sLine = sSkillSource:sub(nIndex, nNextIndex-1)
		if sLine:upper():find("[%w%s+%-*%[%]()//\;&']+[:%s]+D%d+[+%-]?%d*") then
			sSkillLine = sSkillLine .. " " .. sLine
		else
			return sSkillLine
		end
		nIndex = nNextIndex+1
	end
	return sSkillLine
end

function includeEntry(aEntries, sEntry)
	if aEntries and sEntry then
		sEntry = removeLinebreaks(sEntry):match("(%w.+)") or ""
		sEntry = StringManager.trim(sEntry):gsub("%.$", "")
		if StringManager.isNotBlank(sEntry) then
			table.insert(aEntries, StringManager.capitalize(sEntry))
		end
	end
end

function getOtherListLine(sTitle, sSource)
	local sListLine = ""
	local _, _, nListLineStart = CommonParser.getTerminatorFragment(sSource, sTitle)
	if not nListLineStart then
		return ""
	end
	local sOtherSource = sSource:sub(nListLineStart+1) .. "\n"
	local nIndex = 0
	local nUnclosedParenthesis = 0
	while(sOtherSource:find("\n", nIndex)) do
		local nNextIndex = sOtherSource:find("\n", nIndex)
		local sLine = sOtherSource:sub(nIndex, nNextIndex-1)
		if sLine:find(reListStart) and nUnclosedParenthesis < 1 then
			sListLine = sListLine:gsub("%.$", "")
			return sListLine, nListLineStart, nNextIndex+1
		else
			sListLine = sListLine .. (StringManager.isBlank(sListLine) and "" or " ") .. sLine:gsub("\r", "")
		end
		nUnclosedParenthesis = nUnclosedParenthesis + CommonParser.getParenthesisState(sLine)
		nIndex = nNextIndex + 1
	end
	sListLine = sListLine:gsub("%.$", "")
	return sListLine, nListLineStart, #sSource
end

function getOtherListLineEntries(sTitle, sSource, sSeparator)
	local aList = {}
	local sLine, nListLineStart = getOtherListLine(sTitle, sSource)
	if StringManager.isNotBlank(sLine) then
		for _,sEntry in pairs(StringManager.split(sLine, sSeparator and sSeparator or ",")) do
			includeEntry(aList, sEntry)
		end
	end
	return aList, nListLineStart
end

function getPowers(sSource)
	local aArcaneBackgrounds = {}
	local sPowerPointPatterns = { "Power Points:", "PP:", "PPE:", "ISP:" }
	local getPowerPoints = function(sContent)
		sContent = sContent:gsub("Power[\n\r%s]+Points:", "Power Points:")
	    for _,sMatcher in pairs(sPowerPointPatterns) do
	        if StringManager.isNotBlank(sMatcher) then
	        	local sPowerPoints = StatBlockParser.getOtherListLine(sMatcher, sContent)
				if not StringManager.isBlank(sPowerPoints) and sPowerPoints:find("%d+") then
					return tonumber(sPowerPoints:match("(%d+)"))
				end
	        end
	    end
	end
	function include(aPowers, rArcaneBackground)
		if #aPowers > 0 then
			rArcaneBackground.aPowers = {}
			for _,sPower in pairs(aPowers) do
				for _,sRe in pairs(sPowerPointPatterns) do
					sPower = sPower:gsub(sRe .. "%s*%d+%s*$", "")
				end
				sPower = sPower:gsub("%.%s*$", "")
				table.insert(rArcaneBackground.aPowers, sPower)
			end
			table.insert(aArcaneBackgrounds, rArcaneBackground)
		end
	end
	local getPowerLineEntries = function(sPrefix, sContent)
		local aPowers = {}
		local aEntries, nPowersStart = StatBlockParser.getOtherListLineEntries(sPrefix, sContent)
		for _,sEntry in pairs(aEntries) do
			local sPower = sEntry:gsub("%..+$", "")
			table.insert(aPowers, sPower)
		end
		return aPowers, nPowersStart
	end

	-- Common
	local aPowers, nPowersStart = getPowerLineEntries("Powers:", sSource)
	include(aPowers, { 
		powerpointsmax = getPowerPoints(sSource) 
	})
	-- Rifts
	local nIndex = nPowersStart or 0
	local sRePower = "Powers%s*%(([^%)]+)%):"
	while sSource:find(sRePower, nIndex) do
		local nTitleStart, nTitleEnd = sSource:find(sRePower, nIndex)
		local sContent = sSource:sub(nIndex)
		local aPowers, nPowersStart = getPowerLineEntries(sRePower, sContent)
		local sName = StringManager.trim(sSource:sub(nTitleStart, nTitleEnd):match(sRePower))
		include(aPowers, {
			name = sName,
			powerpointsmax = getPowerPoints(sContent:sub(nPowersStart)),
			nodeRecord = CommonParser.findLibraryRecordByName("arcanebackground", sName)
		})
		nIndex = nTitleEnd
	end
	-- SWPF
	local sRePower = "([^\n\r]+)%s+Powers:"
	while sSource:find(sRePower, nIndex) do
		local nTitleStart, nTitleEnd = sSource:find(sRePower, nIndex)
		local sContent = sSource:sub(nIndex)
		local aPowers, nPowersStart = getPowerLineEntries(sRePower, sContent)
		local sName = StringManager.trim(sSource:sub(nTitleStart, nTitleEnd):match(sRePower))
		include(aPowers, {
			name = sName,
			powerpointsmax = getPowerPoints(sContent:sub(nPowersStart)),
			nodeRecord = CommonParser.findLibraryRecordByName("arcanebackground", sName)
		})
		nIndex = nTitleEnd
	end
	return aArcaneBackgrounds
end

function getSpecialAbilitiesLines(sSource)
	local _,nSpecialAbilitiesLabelStart, nSpecialAbilitiesStart = CommonParser.getTerminatorFragment(sSource, reSpecialAbilitiesLabel)
	if not nSpecialAbilitiesStart then
		return {}
	end
	local sSpecialAbilitySource = sSource:sub(nSpecialAbilitiesStart+1) .. "\n"
	local aSpecialAbilityLines = {}
	local sSpecialAbilityLine = ""
	local nIndex = 0
	local nUnclosedParenthesis = 0
	while(sSpecialAbilitySource:find("\n", nIndex)) do
		local nNextIndex = sSpecialAbilitySource:find("\n", nIndex)
		local sLine = sSpecialAbilitySource:sub(nIndex, nNextIndex-1)
		if sLine:find("[^:]+:") and nUnclosedParenthesis < 1 then
			includeEntry(aSpecialAbilityLines, sSpecialAbilityLine)
			sSpecialAbilityLine = sLine
		else
			sSpecialAbilityLine = sSpecialAbilityLine .. " " .. sLine
		end
		nUnclosedParenthesis = nUnclosedParenthesis + CommonParser.getParenthesisState(sLine)
		nIndex = nNextIndex + 1
	end
	includeEntry(aSpecialAbilityLines, sSpecialAbilityLine)
	return aSpecialAbilityLines, nSpecialAbilitiesLabelStart
end

function getSuperPowersLines(sSource)
	local _,nSuperPowersLabelStart, nSuperPowersStart = CommonParser.getTerminatorFragment(sSource, reSuperPowersLabel)
	if not nSuperPowersStart then
		return {}
	end
	local sSuperPowerSource = sSource:sub(nSuperPowersStart+1) .. "\n"
	local aSuperPowerLines = {}
	local sSuperPowerLine = ""
	local nIndex = 0
	local nUnclosedParenthesis = 0
	while(sSuperPowerSource:find("\n", nIndex)) do
		local nNextIndex = sSuperPowerSource:find("\n", nIndex)
		local sLine = sSuperPowerSource:sub(nIndex, nNextIndex-1)
		if sLine:find("[^:]+:") and nUnclosedParenthesis < 1 then
			includeEntry(aSuperPowerLines, sSuperPowerLine)
			sSuperPowerLine = sLine
		else
			sSuperPowerLine = sSuperPowerLine .. " " .. sLine
		end
		nUnclosedParenthesis = nUnclosedParenthesis + CommonParser.getParenthesisState(sLine)
		nIndex = nNextIndex + 1
	end
	includeEntry(aSuperPowerLines, sSuperPowerLine)
	return aSuperPowerLines, nSuperPowersLabelStart
end

function getCommaSeparatedLines(sTitle, sSource)
	local aList = {}
	for sEntity in string.gmatch(getOtherListLine(sTitle, sSource)..",", "([%w%s+%-%[%]();//\&']+),") do
		sEntity = StringManager.trim(sEntity or "")
		if sEntity ~= "" then
			table.insert(aList, sEntity)
		end
	end
	return aList
end
