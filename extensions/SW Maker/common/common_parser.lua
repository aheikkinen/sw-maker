--
-- Please see the license.html file included with this distribution for
-- attribution and copyright information.
--

function createRecordsFromClipboardData(fMaker, vTarget)
	local sSource = Interface.clipboardGetText()
	local aSourceList = {}
	if sSource then
		local nIndex = 0
		local reSeparator = "##########+[%s%t]*"
		if sSource:find(reSeparator) then
			while(sSource:find(reSeparator, nIndex)) do
				local nEnd, nNextIndex = sSource:find(reSeparator, nIndex)
				local sSourceFragment = sSource:sub(nIndex, nEnd-1)
				if StringManager.trim(sSourceFragment) ~= "" then
					table.insert(aSourceList, sSourceFragment)
				end
				nIndex = nNextIndex + 1
			end
		end
		local sRemainingSourceFragment = sSource:sub(nIndex)
		if StringManager.trim(sRemainingSourceFragment or "") ~= "" then
			table.insert(aSourceList, sRemainingSourceFragment)
		end
	end
	if #aSourceList < 1 then
		Comm.addChatMessage({
			font = "systemfont", 
			icon = "poll_negative",
			text = "WARNING! Clipboard is empty, cannot make Record(s)"
		})
		return false
	elseif #aSourceList == 1 then
		Comm.addChatMessage({
			font = "systemfont", 
			icon = "poll_empty",
			text = "Converting following stat blocks into Record:" 
		})
		Comm.addChatMessage({
			mode = "story", 
			font = "chatfont",
			text = aSourceList[1]
		})
	elseif #aSourceList > 1 then
		Comm.addChatMessage({
			font = "systemfont", 
			icon = "poll_empty",
			text = "Converting ".. #aSourceList .." stat blocks into Record(s):"
		})
	end
	for _,sSourceFragment in pairs(aSourceList) do
		if #aSourceList > 1 then
			Comm.addChatMessage({
				mode = "story", 
				font = "chatfont",
				text = sSourceFragment:sub(1,100) .. "..."
			})
		end
		fMaker(sSourceFragment, vTarget, #aSourceList > 1)
	end
	if #aSourceList > 1 then
		Comm.addChatMessage({ 
			font = "systemfont", icon = "poll_check",
			text = "Done! You can find created Records (".. #aSourceList ..") in Record list now", 
			shortcuts = { { class = "masterindex", recordname = "npc" } }
		})
	end
end

function rebuildRecordsFromSource(fMarker, sPath)
	local nCount = 0
	for _,node in pairs(DB.getChildren(sPath)) do
		local sSource = DB.getValue(node, "swmaker-source")
		if sSource then
			DB.deleteChildren(node.getNodeName())
			fMarker(sSource, node, true)
			nCount = nCount + 1
		end
	end
	Comm.addChatMessage({text=nCount .. " Record(s) updated", mode="story", font="chatfont", icon="poll_check"})
end

function cleanBuildRecords(sPath, vFilter)
	local nCount = 0
	for _,nodeRecord in pairs(DB.getChildren("npc")) do
		local nodeSource = nodeRecord.getChild("swmaker-source")
		if nodeSource then
			local sCategory = nodeRecord.getCategory()
			if not sCategory or StringManager.equals(sCategory.name, vFilter) then
				nodeSource.delete()
				nCount = nCount + 1
			end
		end
	end
	Comm.addChatMessage({text=nCount .. " Record(s) cleaned up", mode="story", font="chatfont", icon="poll_check"})
end

function removeBuildRecords(sPath, vFilter)
	local nCount = 0
	for _,nodeRecord in pairs(DB.getChildren(sPath)) do
		local nodeSource = nodeRecord.getChild("swmaker-source")
		if nodeSource then
			local sCategory = nodeRecord.getCategory()
			if not sCategory or StringManager.equals(sCategory.name, vFilter) then
				nodeRecord.delete()
				nCount = nCount + 1
			end
		end
	end
	Comm.addChatMessage({text=nCount .. " Record(s) removed", mode="story", font="chatfont", icon="poll_check"})
end

function recordSynonyms()
	return {
		{ title = "Boost trait", synonyms = { "Boost/Lower Trait" } },
		{ title = "Lower Trait", synonyms = { "Boost/Lower Trait" } },
		{ title = "Detect Arcana", synonyms = { "Detect/Conceal Arcana" } },
		{ title = "Detect Arcane", synonyms = { "Detect/Conceal Arcana" } },
		{ title = "Conceal Arcana", synonyms = { "Detect/Conceal Arcana" } },
		{ title = "Conceal Arcane", synonyms = { "Detect/Conceal Arcana" } },
		{ title = "Growth", synonyms = { "Growth/Shrink" } },
		{ title = "Shrink", synonyms = { "Growth/Shrink" } },
		{ title = "Light", synonyms = { "Light/Darkness" } },
		{ title = "Obscure", synonyms = { "Light/Darkness" } },
		{ title = "Darkness", synonyms = { "Light/Darkness" } },
		{ title = "Sound", synonyms = { "Sound/Silence" } },
		{ title = "Silence", synonyms = { "Sound/Silence" } },
		{ title = "Sloth", synonyms = { "Sloth/Speed" } },
		{ title = "Speed", synonyms = { "Sloth/Speed" } },
		{ title = "Heavy Armor", synonyms = { "Armor" } },
		{ title = "Bite", synonyms = { "Natural Weapons" } },
		{ title = "Claw", synonyms = { "Natural Weapons" } },
		{ title = "Claws", synonyms = { "Natural Weapons" } },
		{ title = "Horn", synonyms = { "Horns/Tusks", "Natural Weapons" } },
		{ title = "Horns", synonyms = { "Horns/Tusks", "Natural Weapons" } },
		{ title = "Tusk", synonyms = { "Horns/Tusks", "Natural Weapons" } },
		{ title = "Tusks", synonyms = { "Horns/Tusks", "Natural Weapons" } },
		{ title = "Stampede", synonyms = { "Stampedes" } },
		{ title = "Stampedes", synonyms = { "Stampedes" } },
		{ title = "Drilling Horns", synonyms = { "Natural Weapons" } },
		{ title = "Ramming Drills", synonyms = { "Natural Weapons" } },
		{ title = "Claw/Bite", synonyms = { "Natural Weapons" } },
		{ title = "Claws/Bite", synonyms = { "Natural Weapons" } },
		{ title = "Bite/Claw", synonyms = { "Natural Weapons" } },
		{ title = "Bite/Claws", synonyms = { "Natural Weapons" } },
		{ title = "Bony Claws", synonyms = { "Natural Weapons" } },
		{ title = "Dodge (Imp)", synonyms = { "Improved Dodge" } },
		{ title = "Frenzy (Imp)", synonyms = { "Improved Frenzy" } },
		{ title = "Sweep (Imp)", synonyms = { "Improved Sweep" } },
		{ title = "Block (Imp)", synonyms = { "Improved Block" } },
		{ title = "Sweep (Imp)", synonyms = { "Improved Sweep" } },
		{ title = "Nerves of Steel (Imp)", synonyms = { "Improved Nerves of Steel" } },
		{ title = "Trademark Weapon (Imp)", synonyms = { "Improved Trademark Weapon" } },
		{ title = "Fan the Hammer (Imp)", synonyms = { "Improved Fan the Hammer" } },
		{ title = "Level Headed (Imp)", synonyms = { "Improved Level Headed" } },
		{ title = "Fiery Breath", synonyms = { "Breath Weapons" } },
		{ title = "Fast Regeneration", synonyms = { "Regeneration" } },
		{ title = "Slow Regeneration", synonyms = { "Regeneration" } },
		{ title = "Burrow", synonyms = { "Burrowing" } },
		{ title = "Rabble Rouser", synonyms = { "Rabble Rouse" } },
		{ title = "Hexslinging", synonyms = { "Hexslinger" } },
		{ title = "Holy Warrior", synonyms = { "Holy/Unholy Warrior" } },
		{ title = "Unholy Warrior", synonyms = { "Holy/Unholy Warrior" } },
		{ title = "Languages", synonyms = { "Language" } }
	}
end

function preFormat(sSource)
	if not sSource then
		return 
	end
	sSource = sSource:gsub("@@[^:]+::", ""):gsub("\"\"\"", ""):gsub("%[%[", ""):gsub("%]%]", "")
	return sSource
end

function capitalizeString(s)
	local capitalized = function(s)
		local sContent = ""
		for _,sPart in pairs(StringManager.split(s, "%s")) do
			sContent = StringManager.append(sContent, StringManager.capitalize(sPart), " ")
		end
		return sContent
	end
	if not s then
		return ""
	elseif s:upper() == s then
		return capitalized(s:lower())
	elseif s:lower() == s then
		return capitalized(s)
	else
		return s
	end
end

function formatText(sContent)
	if StringManager.isNotBlank(sContent) then
		if RulesetManager.isExtensionsEnabled("Module Maker") then
			return ModuleGenerator.formatContent(sContent)
		elseif RulesetManager.isExtensionsEnabled("Enhanced Library") then
			return LibraryContentImporter.formatContent(sContent)
		end
	end
	return sContent
end

function plainText(sContent)
	if StringManager.isNotBlank(sContent) then
		if RulesetManager.isExtensionsEnabled("Module Maker") then
			return ModuleGenerator.stripFormatDelims(sContent):gsub("\n", " ")
		elseif RulesetManager.isExtensionsEnabled("Enhanced Library") then
			return LibraryContentImporter.stripFormatDelims(sContent):gsub("\n", " ")
		end
	end
	return sContent
end

function getTerminatorFragment(sSource, sPrefix)
	local nFragmentStart, nFragmentEnd = sSource:lower():find(sPrefix:lower())
	if not nFragmentEnd then
		return ""
	end
	local nLineBreak = sSource:find("\n", nFragmentEnd+1)
	if nLineBreak then
		return StringManager.trim(sSource:sub(nFragmentEnd+1, nLineBreak-1)), nFragmentStart-1, nFragmentEnd+1
	else
		return StringManager.trim(sSource:sub(nFragmentEnd+1)), nFragmentStart-1, nFragmentEnd+1
	end
end

function getParenthesisState(sSource)
	local nUnclosedParenthesis = 0
	for nIndex=1, #sSource do
		local sChar = sSource:sub(nIndex, nIndex)
		if sChar:find("[%(%[{]") then
			nUnclosedParenthesis = nUnclosedParenthesis + 1
		elseif sChar:find("[%)%]}]") then
			nUnclosedParenthesis = nUnclosedParenthesis - 1
		end
	end
	return nUnclosedParenthesis
end

function writeRecordLink(node, sRecordTypes, sLinkClass, sRecordName, bLocalFallback)
	if node then
		for _,sRecordType in pairs(StringManager.split(sRecordTypes, "|")) do
			local nodeRecord = findLibraryRecordByName(sRecordType, sRecordName)
			if nodeRecord then
				DB.setValue(node, "link", "windowreference", sLinkClass, nodeRecord.getNodeName())
				if sLinkClass == "sw_referencefeat" then
					local sRecordEffects = EffectManager.getEffectKeywords(DB.getValue(nodeRecord, "description", ""))
					if StringManager.isNotBlank(sRecordEffects) then
						local sDesc = DB.getValue(node, "description", "")
						DB.setValue(node, "description", "string", StringManager.append("[" .. sRecordEffects .. "]", sDesc, " "))
					end
				end
				return nodeRecord
			end
		end
		print("[WARN] Record with name [" .. sRecordName .."] was not found" .. (bLocalFallback and ". Using local reference" or ""))
		if bLocalFallback then
			DB.setValue(node, "link", "windowreference", sLinkClass, node.getNodeName())
		end
	end
end

function findLibraryRecordByName(sRecordType, sRecordName)
	local equalsIgnoreNumbersAndBrackets = function(s1, s2)
		local stripNumbers = function(s)
			return tostring(s or ""):gsub("[%+%-]?%d+", "")
		end
		return StringManager.equalsIgnoreBrackets(stripNumbers(s1), stripNumbers(s2))
	end
	return LibraryDataSW.findLibraryRecordByName(sRecordType, sRecordName, { StringManager.equals, equalsIgnoreNumbersAndBrackets }, recordSynonyms())
end
