
# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog],
and this project adheres to [Semantic Versioning].

## [Unreleased]

## [2.3.4] - 2024-07-07

### Changed

- Define reach value based on size

## [2.3.3] - 2024-05-19

### Fixed

- NPC creation have script error regarding `initializeAttackTypes`. Credits and kudos to Mike Serfass

## [2.3.2] - 2023-04-16

### Changed

- Provide full reference of arcane background instead of minimal

## [2.3.1] - 2023-04-15

### Fixed

- Last power can have power points label included

## [2.3.0] - 2023-04-15

### Changed

- Multiple arcane background support
- Improved power parsing support for Savage Pathfinder
- Improved power parsing support for Rifts
- Power Points does not need to be in own line
- Respect NPC arcane background ability
- Class ability parsing improved
- Language parsing improved
- Remove redundant dot at the end of list entries

### Fixed

- Type field content is in description

## [2.2.2] - 2023-04-12

### Added

- Conviction support

## [2.2.0] - 2023-04-04

### Added

- Module Maker support

## 2.0.2

### Added

- Unarmed attack is added if stats does not define special ability attack

### Changed

- Improved record matching

## 2.0.1

### Changed

- FGU encoding support improved

## 2.0.0

### Added

- SWEL markdown support

### Changed

- Rebranded to Savage Worlds Maker

## Fixed

- Unskilled encoding in FGC

---

[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html

[Unreleased]: https://bitbucket.org/aheikkinen/sw-maker/branches/compare/main%0D2.3.4
[2.3.4]: https://bitbucket.org/aheikkinen/sw-maker/branches/compare/2.3.4%0D2.3.3
[2.3.3]: https://bitbucket.org/aheikkinen/sw-maker/branches/compare/2.3.3%0D2.3.2
[2.3.2]: https://bitbucket.org/aheikkinen/sw-maker/branches/compare/2.3.2%0D2.3.1
[2.3.1]: https://bitbucket.org/aheikkinen/sw-maker/branches/compare/2.3.1%0D2.3.0
[2.3.0]: https://bitbucket.org/aheikkinen/sw-maker/branches/compare/2.3.0%0D2.2.2
[2.2.2]: https://bitbucket.org/aheikkinen/sw-maker/commits/tag/2.2.2